define(function (){
	var PictureResize = {
		limitWidth : function(file, maxWidth, callback){
			if(!file.type.match(/image.*/)) {
				callback(file);
				return;
			}

			var reader = new FileReader();
			reader.onload = function (readerEvent) {
				var image = new Image();
				image.onload = function (imageEvent) {
					EXIF.getData(file, function(){
						var imageWidth = image.width;
						var imageHeight = image.height;
						var canvas = document.createElement('canvas');

						var orientation = this.exifdata.Orientation;
						var isRotated = false;
						switch(orientation){
							case 8:
								isRotated = true;
								break;
							case 6:
								isRotated = true;
								break;
						}

						if(isRotated){
							if(imageHeight > maxWidth) {
								imageWidth 	= imageWidth * maxWidth / imageHeight;
								imageHeight = maxWidth;
							}

							canvas.width = imageHeight;
							canvas.height = imageWidth;
						}else{
							if(imageWidth > maxWidth) {
								imageHeight = imageHeight * maxWidth / imageWidth;
								imageWidth 	= maxWidth;
							}

							canvas.width = imageWidth;
							canvas.height = imageHeight;
						}

						var ctx = canvas.getContext('2d');
						ctx.save();
						ctx.translate(canvas.width/2,canvas.height/2);
						switch(orientation){
							case 8:
								ctx.rotate(-90*Math.PI/180);
								break;
							case 3:
								ctx.rotate(180*Math.PI/180);
								break;
							case 6:
								ctx.rotate(90*Math.PI/180);
								break;
						}
						ctx.drawImage(image, -imageWidth/2, -imageHeight/2, imageWidth, imageHeight);
						ctx.restore();

						var dataUrl = canvas.toDataURL('image/jpeg');
						var resizedImage = PictureResize.dataURLToBlob(dataUrl);

						callback(resizedImage);
					});
				}
				image.src = readerEvent.target.result;
			}
			reader.readAsDataURL(file);
		},

		getDimensions : function(file, callback){
			if(!file.type.match(/image.*/)) {
				callback(file);
				return;
			}

			var reader = new FileReader();
			reader.onload = function (readerEvent) {
				var image = new Image();
				image.onload = function (imageEvent) {
					EXIF.getData(file, function(){
						var imageWidth = image.width;
						var imageHeight = image.height;
						var canvas = document.createElement('canvas');

						var orientation = this.exifdata.Orientation;
						var rotations = [6, 8];
						var isRotated = (rotations.indexOf(orientation) > -1);
						

						var width 	= isRotated ? imageHeight 	: imageWidth;
						var height 	= isRotated ? imageWidth 	: imageHeight;
						var dimensions = {
							width 	: width,
							height 	: height
						};
						callback(dimensions);
					});
				}
				image.src = readerEvent.target.result;
			}
			reader.readAsDataURL(file);
		},

		drawInCanvas : function(file, canvas, callback){
			if(!file.type.match(/image.*/)) {
				console.log('File is not an Image');
				if(callback) callback();
				return;
			}

			var reader = new FileReader();
			reader.onload = function (readerEvent) {
				var image = new Image();
				image.onload = function (imageEvent) {
					var imageWidth = image.width;
					var imageHeight = image.height;
					var canvasWidth = canvas.width;
					var canvasHeight = canvas.height;
					var width, height;

					var ctx = canvas.getContext('2d');
					ctx.save();
					ctx.translate(canvas.width/2, canvas.height/2);

					EXIF.getData(file, function(){
						var orientation = this.exifdata.Orientation;
						switch(orientation){
							case 8:
								ctx.rotate(-90*Math.PI/180);
								var interm = canvasWidth;
								canvasWidth = canvasHeight;
								canvasHeight = interm;
								break;
							case 3:
								ctx.rotate(180*Math.PI/180);
								break;
							case 6:
								ctx.rotate(90*Math.PI/180);
								var interm = canvasWidth;
								canvasWidth = canvasHeight;
								canvasHeight = interm;
								break;
						}

						if(imageWidth > canvasWidth || imageHeight > canvasHeight){
							if(imageWidth/canvasWidth > imageHeight/canvasHeight){
								width = canvasWidth;
								height = imageHeight * canvasWidth / imageWidth;
							}else{
								height = canvasHeight;
								width = imageWidth * canvasHeight / imageHeight;
							}
						}

						ctx.drawImage(image, -width/2, -height/2, width, height);
						ctx.restore();
						if(callback) callback();
					});
				}
				image.src = readerEvent.target.result;
			}
			reader.readAsDataURL(file);
		},
		drawUrlInCanvas : function(url, canvas, dimensions, callback, crossOrigin){
			if(!url) return console.log('Ungiven url', url);
			if(!dimensions || !dimensions.width || !dimensions.height) return console.log('Ungiven dimensions', dimensions);

			if(!canvas) canvas = document.createElement('canvas');
			var image = new Image();
				image.onload = function (imageEvent) {
					var imageWidth 	= image.width;
					var imageHeight = image.height;

					if(!imageWidth || !imageWidth) return console.log('Wrong image size');

					var resizedWidth, resizedHeight, projectedWidth, projectedHeight;

					if(imageWidth / dimensions.width > imageHeight / dimensions.height){
						var resizedHeight 	= dimensions.height;
						var resizedWidth 	= imageWidth * resizedHeight / imageHeight;

						canvas.height 	= dimensions.height;
						canvas.width 	= Math.min(resizedWidth, dimensions.width);

						var projectedHeight = imageHeight;
						var projectedWidth 	= canvas.width * projectedHeight / canvas.height;
					}else{
						var resizedWidth 	= dimensions.width;
						var resizedHeight 	= imageHeight * resizedWidth / imageWidth;

						canvas.width 	= dimensions.width;
						canvas.height 	= Math.min(resizedHeight, dimensions.height);

						var projectedWidth 	= imageWidth;
						var projectedHeight = canvas.height * projectedWidth / canvas.width;
					}

					var ctx = canvas.getContext('2d');
						ctx.drawImage(image, (imageWidth - projectedWidth) / 2, (imageHeight - projectedHeight) / 2, projectedWidth, projectedHeight, 0, 0, canvas.width, canvas.height);
						var dataUrl = canvas.toDataURL('image/jpeg');
						if(callback) callback(dataUrl);
				};
				if(crossOrigin) image.crossOrigin = 'anonymous';
				image.src = url;
		},

		extractVideoFrame : function(url){
			return new Promise(function(resolve, reject){
				var video 	= document.createElement('video');
				var canvas 	= document.createElement('canvas');
				var ctx 	= canvas.getContext('2d');

				video.addEventListener('loadedmetadata', function(){
					canvas.width 	= this.videoWidth;
					canvas.height 	= this.videoHeight;
				}, false);
				video.addEventListener('timeupdate', function(){
					this.pause();
					ctx.drawImage(this, 0, 0);
					return resolve(canvas.toDataURL()); 
				}, false);

				video.muted 	= true;
				video.autoplay 	= true;
				video.src 		= url;
			});
		},

		dataURLToBlob : function(dataURL){
			var BASE64_MARKER = ';base64,';
			if (dataURL.indexOf(BASE64_MARKER) == -1) {
				var parts = dataURL.split(',');
				var contentType = parts[0].split(':')[1];
				var raw = parts[1];

				return new Blob([raw], {type: contentType});
			}

			var parts = dataURL.split(BASE64_MARKER);
			var contentType = parts[0].split(':')[1];
			var raw = window.atob(parts[1]);
			var rawLength = raw.length;

			var uInt8Array = new Uint8Array(rawLength);

			for (var i = 0; i < rawLength; ++i) {
				uInt8Array[i] = raw.charCodeAt(i);
			}

			return new Blob([uInt8Array], {type: contentType});
		},
		
		URLtoBlob : function(url, callback){
			var blob = null;
			var xhr = new XMLHttpRequest();
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.onload = function(){
				return callback(xhr.response);
			};
			xhr.send();
		}
	};

	return PictureResize;
});